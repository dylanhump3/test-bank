package com.zetapsinu.testbank.dao.accesstoken;

import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.service.UserService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by dylanhumphrey on 2/23/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/context-test.xml"})
public class JpaAccessTokenDaoTest{

    @Autowired
    private UserService userService;

    private final static String TOKEN = "thetokenstring";

    @Before
    public void saveToken(){
        userService.getAccessTokenDao().save(new AccessToken(null, TOKEN));
    }

    @Test
    public void testFindByToken() throws Exception{
        AccessToken token = userService.getAccessTokenDao().findByToken(TOKEN);
        assertNotNull(token);

        assertEquals(TOKEN, token.getToken());
    }

    @Test
    public void testFindByTokenFail() throws Exception{
        AccessToken token = userService.getAccessTokenDao().findByToken("NDOAFIJDS");
        assertNull(token);
    }
}
