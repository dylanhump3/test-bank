package com.zetapsinu.testbank.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * This class is an Entity which represents an access token for a specific user
 * Each user gets an access token upon login and this access token is used to verify users
 */
@javax.persistence.Entity
public class AccessToken implements Entity
{
    /**
     * The uuid of the entity
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The actual body of the access token
     */
    @Column(nullable = false)
    private String token;

    /**
     * This will be encoded in the data base as a user id
     * Many to one because many keys in the AccessToken table could correspond to a single key in the User table
     * ie. a user can have many access tokens, but an access token cant be assigned to more than one user
     */
    @ManyToOne
    private User user;

    /**
     * Provides support for expiration of access tokens so the database doesnt get over crowded
     */
    //TODO: Add support for this by deleting accesstokens from SQL that have already expire
    @Column
    private Date expiry;

    protected AccessToken()
    {
        /* Reflection instantiation */
    }

    /**
     * Default constructer
     * @param user The user this token corresponds to
     * @param token The generated token string
     */
    public AccessToken(User user, String token)
    {
        this.user = user;
        this.token = token;
    }

    /**
     * Constructor to allow expiration date to be set on top of the default constructor
     * @param user The user this token corresponds to
     * @param token The generated token string
     * @param expiry The expiration date of this token
     */
    public AccessToken(User user, String token, Date expiry)
    {
        this(user, token);
        this.expiry = expiry;
    }

    /** Just a bunch of getters and setters for the AccessToken
     */
    @Override
    public Long getId()
    {
        return this.id;
    }

    public String getToken()
    {
        return this.token;
    }

    public User getUser()
    {
        return this.user;
    }

    public Date getExpiry()
    {
        return this.expiry;
    }

    public boolean isExpired()
    {
        if (null == this.expiry) {
            return false;
        }

        return this.expiry.getTime() > System.currentTimeMillis();
    }
}
