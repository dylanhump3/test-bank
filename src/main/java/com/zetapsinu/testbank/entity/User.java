package com.zetapsinu.testbank.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This is the user entity
 * Implements spring UserDetails for security purposes
 */
@javax.persistence.Entity
public class User implements com.zetapsinu.testbank.entity.Entity, UserDetails
{
    /**
     * The uuid of a User
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * The username for the user ie. dch77
     */
    @Column(unique = true, length = 16, nullable = false)
    private String name;

    /**
     * The Full name of the user ie. Dylan Humphrey
     */
    @Column(unique = true, nullable = false)
    private String full_name;

    /**
     * The password for the user
     */
    @Column(length = 80, nullable = false)
    private String password;

    /**
     * If the user is enabled or not
     */
    @Column
    private int enabled;

    /**
     * Creates an element collection which will generate a Role table in SQL
     * Also creates a foreign key which is the User_id in the role table
     */
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<Role>();

    protected User()
    {
        /* Reflection instantiation */
    }

    /**
     * Default constructor
     * @param name The username of the user
     * @param passwordHash THe password which will be encrypted using Bcrypt algorithm
     */
    public User(String name, String passwordHash)
    {
        this.name = name;
        this.password = passwordHash;
    }

    /**
     * All the getters and setters
     */
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Set<Role> getRoles()
    {
        return this.roles;
    }

    public void clearRoles(){
        this.roles = new HashSet<>();
    }

    public void setRoles(Set<Role> roles)
    {
        this.roles = roles;
    }

    public void addRole(Role role)
    {
        this.roles.add(role);
    }

    public void addRoles(Role... roles){
        this.roles.addAll(Arrays.asList(roles));
    }

    @Override
    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return this.getRoles();
    }

    @Override
    public String getUsername()
    {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled ? 1 : 0;
    }

    @Override
    public boolean isEnabled()
    {
        return this.enabled == 1;
    }
}
