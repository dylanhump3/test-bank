package com.zetapsinu.testbank.entity;

import java.io.Serializable;

/**
 * Highest level for Entity
 * All of my entities implement this interface
 */
public interface Entity extends Serializable
{
    Long getId();
}
