package com.zetapsinu.testbank.entity;

import org.springframework.security.core.GrantedAuthority;

/**
 * This is not an entity but is stored in SQL via the user table
 * @see User
 */
public enum Role implements GrantedAuthority
{
    /**
     * Define all different permission types here
     */
    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN");

    //The authority of the user
    private String authority;

    //package local constructor for a Role
    Role(String authority)
    {
        this.authority = authority;
    }

    /**
     * Overrides the GrantedAuthority getAuthority() method
     * @return The authority for this role such as "ROLE_USER"
     */
    @Override
    public String getAuthority()
    {
        return this.authority;
    }

    /**
     *
     * @return
     */
    public int toInt(){
        if (this == USER){
            return 0;
        }else if (this == ADMIN){
            return 1;
        }
        return -1;
    }
}
