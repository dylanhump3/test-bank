package com.zetapsinu.testbank.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.log4j.Logger;
import com.zetapsinu.testbank.model.FileList;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Created by dylanhumphrey on 1/23/17.
 * This is a global cache that will hold the list of all files in the s3 database
   stored in my FileList data structure
 * @see FileList
 * Also stores all the users from
 */

@Component
public class DataCache {

    /*The cache uses a google cache to store the data*/
    private Cache<String, String> cache;

    /*Default settings*/
    private final int size = 10;
    private final int time = 30;

    /*For logging purposes*/
    private final Logger logger = Logger.getLogger(DataCache.class);

    /*Private instance method used by the Singleton*/
    private DataCache(){
        cache = CacheBuilder.newBuilder()
                .maximumSize(size)
                .expireAfterAccess(time, TimeUnit.MINUTES)
                .build();
        logger.info("Initialized DataCache");
    }

    public Cache<String, String> getCache(){
        return cache;
    }

    /*Uses the Initialization-on-demand holder idiom to store the datacache
    * Represents a thread-safe manor to retrieve an instance of the DataCache
    * on a global scale*/
    private static class Singleton{
        private static final DataCache INSTANCE = new DataCache();
    }

    public static DataCache getInstance(){
        return Singleton.INSTANCE;
    }
}
