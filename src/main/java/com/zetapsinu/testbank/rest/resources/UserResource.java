package com.zetapsinu.testbank.rest.resources;

import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.entity.User;
import com.zetapsinu.testbank.transfer.UserTransfer;
import com.zetapsinu.testbank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

/**
 * A Spring component that provides REST support for the login page on the website
 * All REST endpoints require no authentication is this is the process that will be doing the authentication
 */
@Component
@Path("/user")
public class UserResource
{
    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authManager;

    /**
     * Retrieves the currently logged in user
     * @return A transfer containing the username and the roles
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UserTransfer getUser()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (!(principal instanceof UserDetails)) {
            throw new WebApplicationException(401);
        }
        User userDetails = (User) principal;

        return new UserTransfer(userDetails.getUsername(), this.createRoleMap(userDetails), userDetails.getFull_name());
    }

    /**
     * Authenticates a user and creates an access token
     * @param username The name of the user
     * @param password The password of the user
     * @return The generated access token
     */
    @Path("authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public AccessToken authenticate(@FormParam("username") String username, @FormParam("password") String password)
    {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = this.authManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        Object principal = authentication.getPrincipal();
        if (!(principal instanceof User)) {
            throw new WebApplicationException(401);
        }

        return this.userService.createAccessToken((User) principal);
    }

    /**
     * Helper method that maps the authority of the user object
     * @param userDetails The user information
     * @return A map representing the roles
     */
    private Map<String, Boolean> createRoleMap(UserDetails userDetails)
    {
        Map<String, Boolean> roles = new HashMap<String, Boolean>();
        for (GrantedAuthority authority : userDetails.getAuthorities()) {
            roles.put(authority.getAuthority(), Boolean.TRUE);
        }
        return roles;
    }
}
