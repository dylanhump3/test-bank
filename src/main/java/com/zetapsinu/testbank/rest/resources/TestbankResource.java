package com.zetapsinu.testbank.rest.resources;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.zetapsinu.testbank.JsonViews;
import com.zetapsinu.testbank.cache.DataCache;
import com.zetapsinu.testbank.model.FileTree;
import com.zetapsinu.testbank.model.FileList;

import com.zetapsinu.testbank.service.DaoUserService;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * A Spring component that provides REST support for the testbank page
 * All REST endpoints require USER level authentication
 */
@Component
@PropertySource("classpath:application.properties")
@Path("/testbank")
public class TestbankResource {

    /**
     * Just a logger and the name of the AWS bucket
     */
    private final Logger logger = Logger.getLogger(TestbankResource.class);
    private final String bucketName = "gainz";

    /**
     * We need the AWS client in order to grab the list of files from S3
     */
    @Autowired
    private AmazonS3Client amazonS3Client;

    /**
     * Autowire our object mapper for seralizing the files
     */
    @Autowired
    private ObjectMapper mapper;

    /**
     * Provides a generic query support for getting all the files from s3
     * Doesn't download the actual files here just the names
     * @return A json string representing all the files
     * @throws IOException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String list() throws IOException {
        ObjectWriter viewWriter;

        if (DaoUserService.isCurrentUserAdmin()) {
            viewWriter = this.mapper.writerWithView(JsonViews.Admin.class);
        } else {
            viewWriter = this.mapper.writerWithView(JsonViews.User.class);
        }

        try {
            String json = DataCache.getInstance().getCache().get("testbank", () -> {
                String str = viewWriter.writeValueAsString(getTestBank().serealizeTrees());
                logger.info("Caching AWS request..." + str);
                return str;
            });
            logger.info("AWS request successfully handled. JSON=" + json);
            return json;
        }catch (Exception e){
            throw new WebApplicationException(500);
        }
    }

    /**
     * Utilizes my custom data structure to store the results from s3
     * @see FileList
     * @see FileTree
     * @return A FileList object representing the entire file hierarchy from the s3 database
     */
     private FileList getTestBank(){
        try {
            final ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(bucketName);
            ListObjectsV2Result result = amazonS3Client.listObjectsV2(req);
            logger.info("Handling AWS s3 request: " + req.getBucketName());
            return generateFileHeirarchy(result.getObjectSummaries());
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, " +
                    "which means your request made it " +
                    "to Amazon S3, but was rejected with an error response " +
                    "for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.error("Caught an AmazonClientException, " +
                    "which means the client encountered " +
                    "an internal error while trying to communicate" +
                    " with S3, " +
                    "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
        }
        return null;
    }

    /**
     * Helper method for generating the File Heirarchy
     * @param objectSummaries The objects resulting from the s3 call
     * @return The FileList object
     */
    private FileList generateFileHeirarchy(List<S3ObjectSummary> objectSummaries){
        FileList list = new FileList();
        for (S3ObjectSummary objectSummary : objectSummaries) {
            String key = objectSummary.getKey();
            float size = objectSummary.getSize();

            String[] components = key.split("/");

            //Will be the course
            String course = components[0];

            /* Two Possible cases from here
            * 1. Course has been added yet
            * 2. Course is there, but file has been added yet*/
            if (list.contains(course)){
                list.get(course).addFile(Arrays.asList(components), size);
            }else{
                list.add(new FileTree(Arrays.asList(components), size));
            }
        }
        return list;
    }
}
