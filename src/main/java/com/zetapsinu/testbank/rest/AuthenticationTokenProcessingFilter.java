package com.zetapsinu.testbank.rest;

import com.zetapsinu.testbank.entity.User;
import com.zetapsinu.testbank.service.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * This class intercepts authentication tokens and uses them to set authentication in the current context
 */
public class AuthenticationTokenProcessingFilter extends GenericFilterBean
{
    /**
     * Uses the user service in order to grab users via access tokens
     */
    private final UserService userService;

    /**
     * Default constructor
     * @param userService Will be dependency injected via Spring
     */
    public AuthenticationTokenProcessingFilter(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * Overrides the GenericFilterBean doFilter method in order to authenticate users
     * @param request The Servlet request which is represented as an HTTP request
     * @param response The HTTP response
     * @param chain The filter chain that is described in the context
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        HttpServletRequest httpRequest = this.getAsHttpRequest(request);

        String accessToken = this.extractAuthTokenFromRequest(httpRequest);
        if (null != accessToken) {
            User user = this.userService.findUserByAccessToken(accessToken);
            if (null != user) {
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(request, response);
    }

    /**
     * Helper method that maps a ServletRequest into and HTTP request
     * @param request The servlet request
     * @return The Servlet request as an HTTP request
     */
    private HttpServletRequest getAsHttpRequest(ServletRequest request)
    {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }

        return (HttpServletRequest) request;
    }

    /**
     * Helper method to grab the access token from the HTTP request
     * @param httpRequest The http request containing the token
     * @return The authentication token
     */
    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest)
    {
        /* Get token from header */
        String authToken = httpRequest.getHeader("X-Access-Token");

		/* If token not found get it from request parameter */
        if (authToken == null) {
            authToken = httpRequest.getParameter("token");
        }

        return authToken;
    }
}
