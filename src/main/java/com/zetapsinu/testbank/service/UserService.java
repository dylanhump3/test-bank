package com.zetapsinu.testbank.service;

import com.zetapsinu.testbank.dao.accesstoken.AccessTokenDao;
import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * The top level of the access to the user service
 * Implements UserDetailsService which is defined by Spring
 */
public interface UserService extends UserDetailsService
{
    User findUserByAccessToken(String accessToken);

    AccessToken createAccessToken(User user);

    AccessTokenDao getAccessTokenDao();
}
