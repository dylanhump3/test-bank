package com.zetapsinu.testbank.service;

import com.zetapsinu.testbank.dao.accesstoken.AccessTokenDao;
import com.zetapsinu.testbank.dao.user.UserDao;
import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.entity.Role;
import com.zetapsinu.testbank.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

/**
 * The implementation of the UserService interface which provides the ability to grab users via username and accesstoken
 */
public class DaoUserService implements UserService
{
    /**
     * We need our UserDao dependency injected in order to make changes to the SQL database
     */
    private UserDao userDao;

    /**
     * We need this to grab accesstokens from SQL
     */
    private AccessTokenDao accessTokenDao;

    protected DaoUserService()
    {
        /* Reflection instantiation */
    }

    /**
     * Constructor for the UserService
     * @param userDao Will be dependency injected via Spring
     * @param accessTokenDao Will be dependency injected via Spring
     */
    public DaoUserService(UserDao userDao, AccessTokenDao accessTokenDao)
    {
        this.userDao = userDao;
        this.accessTokenDao = accessTokenDao;
    }

    /**
     * Simple getter for the access token dao
     * @return the access token dao
     */
    public AccessTokenDao getAccessTokenDao() {
        return accessTokenDao;
    }

    /**
     * Find a user by their username
     * @param username The username of the user
     * @return The found user
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        return this.userDao.loadUserByUsername(username);
    }

    /**
     * Uses an access token to find a user
     * @param accessTokenString The access token
     * @return The found user or null if not found
     */
    @Override
    @Transactional
    public User findUserByAccessToken(String accessTokenString)
    {
        AccessToken accessToken = this.accessTokenDao.findByToken(accessTokenString);

        if (null == accessToken) {
            return null;
        }

        if (accessToken.isExpired()) {
            this.accessTokenDao.delete(accessToken);
            return null;
        }

        return accessToken.getUser();
    }

    /**
     * Method to create an access token for a user
     * @param user The user you wish to create the access token for
     * @return The generated accesstoken
     */
    @Override
    @Transactional
    public AccessToken createAccessToken(User user)
    {
        AccessToken accessToken = new AccessToken(user, UUID.randomUUID().toString());
        return this.accessTokenDao.save(accessToken);
    }

    public static boolean isCurrentUserAdmin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if ((principal instanceof String) && ((String) principal).equals("anonymousUser")) {
            return false;
        }
        UserDetails userDetails = (UserDetails) principal;

        return userDetails.getAuthorities().contains(Role.ADMIN);
    }
}
