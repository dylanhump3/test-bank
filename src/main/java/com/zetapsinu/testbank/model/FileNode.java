package com.zetapsinu.testbank.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * Abstract class which is never initialized (definition of abstract)
 * Provides polymorphism ability for all FileNode classes
 * Makes up a file tree
 */
public abstract class FileNode implements Serializable {
    String name;

    public FileNode(String name) {
        this.name = name;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap();
        map.put("name", this.name);
        return map;
    }
}
