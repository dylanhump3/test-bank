package com.zetapsinu.testbank.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * FileList class is just a list of a custom data structure I wrote in order to map the list of files from AWS S3
 * Has some important methods on it that allow for it to act like a list
 * Each file tree represents a course
 * @see FileTree
 */
public class FileList {

    /**
     * The actual list of Trees
     * Private access within class
     */
    private List<FileTree> treeList;

    /**
     * Default constructor
     * Initializes the list
     */
    public FileList() {
        this.treeList = new ArrayList();
    }

    /**
     * Provides ability to add a FileTree to the internal list
     * @param tree The tree to be added
     */
    public void add(FileTree tree) {
        this.treeList.add(tree);
    }

    /**
     * Contains method to determine if a specific course is in the list already
     * @param course
     * @return
     */
    public boolean contains(String course) {
        return get(course) != null;
    }

    public FileTree get(String course) {
        for (FileTree tree : this.treeList) {
            if (tree.root.name.equals(course)) {
                return tree;
            }
        }
        return null;
    }

    public List<Map<String, Object>> serealizeTrees() {
        List<Map<String, Object>> list = new LinkedList();
        for (FileTree tree : this.treeList) {
            list.add(tree.toMap());
        }
        return list;
    }

    public List<FileTree> getTreeList() {
        return this.treeList;
    }
}

