package com.zetapsinu.testbank.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * An implementation of the FileNode Abstract class
 * FileNodeBranch represents a folder
 * Cannot be an end node of the tree
 * This node has the capacity to have infinite children
 * @see FileTree
 */
public class FileNodeBranch extends FileNode {

    /**
     * Use a List to hold the children of this node
     */
    private List<FileNode> children;

    /**
     * Default constructor
     * @param name The name of the this node ie. will be something such as Tests
     */
    public FileNodeBranch(String name) {
        super(name);
        this.children = new ArrayList();
    }

    /**
     * Search children for a node with the name specified
     * @param name The name of the child to find
     * @return A node based on the parameter or null if it doesn't exist
     */
    public FileNode get(String name) {
        for (FileNode node : this.children) {
            if (node.name.equals(name)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Recursive method required for JSON serialization
     * JSON Serialization libraries are unable to process and understand the custom tree
     * This method turns the tree into a hash map
     * @return The map after serialization
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = super.toMap();
        List<Map<String, Object>> list = new ArrayList<>();
        this.children.stream().forEach((child) -> list.add(child.toMap()));
        map.put("children", list);
        return map;
    }

    /**
     * Getters/Setters/Helpers
     */
    public boolean has(String name) {
        return get(name) != null;
    }

    public void addChild(FileNode child) {
        this.children.add(child);
    }

    public List<FileNode> getChildren() {
        return this.children;
    }
}
