package com.zetapsinu.testbank.model;

import java.util.List;
import java.util.Map;

/**
 * The Custom data structure implementation
 * Similar to a Binary Tree with a root node
 * Only difference is there is no organization requried to it and each branch node has infinite children
 */
public class FileTree {

    /**
     * The root node of the tree
     */
    public FileNode root;

    /**
     * Constructor for the tree
     * @param path Takes the entire file path as an array of strings [ENGR 145, Tests, test1.pdf]
     * @param size Takes the size of the file
     */
    public FileTree(List<String> path, Float size) {
        this.root = new FileNodeBranch((String)path.get(0));
        addFile(path, size);
    }

    /**
     * This method works all the magic in adding files and folders to the tree
     * @param path Takes the entire file path as an array of strings [ENGR 145, Tests, test1.pdf]
     * @param size Takes the size of the file
     */
    public void addFile(List<String> path, Float size) {
        FileNode current = this.root;
        for (int i = 1; i < path.size(); i++) {
            String file = (String)path.get(i);
            if (((FileNodeBranch)current).has(file)) {
                current = ((FileNodeBranch)current).get(file);
            }
            else {
                if (file.endsWith("pdf")
                        || file.endsWith("ppt")
                        || file.endsWith("docx")
                        || file.endsWith("rtf")
                        || file.endsWith("doc")
                        || file.endsWith("xlsx")
                        || file.endsWith(".PDF")
                        || file.endsWith(".DS_STORE")) {
                    FileNode node = new FileNodeEnd(file, size);
                    ((FileNodeBranch)current).addChild(node);
                    return;
                }
                FileNode newNode = new FileNodeBranch(file);
                ((FileNodeBranch)current).addChild(newNode);
                current = newNode;
            }
        }
    }

    /**
     * The serialization method for a file tree
     * toMap() only needs to be called on the root node as it is a recursive algorithm which will go down the tree and serialize every node
     * @return A map representing this tree
     * @see FileNodeBranch
     */
    public Map<String, Object> toMap() {
        return this.root.toMap();
    }
}

