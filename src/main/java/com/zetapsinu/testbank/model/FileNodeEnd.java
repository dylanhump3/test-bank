package com.zetapsinu.testbank.model;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Map;

/**
 * An Implementation of the FileNode abstract class
 * Represents an actual file with a size
 * Is a leaf node
 */
public class FileNodeEnd extends FileNode{

    //The size of the file
    Float size;

    /**
     * Constructor for a node
     * @param name The name of the node
     * @param size The size of the file
     */
    public FileNodeEnd(String name, Float size){
        super(name);
        this.size = size;
    }

    /**
     * Implementation of the to map method
     * No children so super simple to serialize
     * @return This node as a map
     */
    public Map<String, Object> toMap() {
        Map<String, Object> map = super.toMap();
        DecimalFormat df = new DecimalFormat("##.##");
        df.setRoundingMode(RoundingMode.HALF_UP);
        map.put("size", df.format(size/1000000));
        return map;
    }
}
