package com.zetapsinu.testbank.transfer;

import java.util.Map;


public class UserTransfer
{

	private final String name;
	private final String fullName;
	private final Map<String, Boolean> roles;


	public UserTransfer(String userName, Map<String, Boolean> roles, String fullName)
	{
		this.name = userName;
		this.fullName = fullName;
		this.roles = roles;
	}


	public String getName()
	{
		return this.name;
	}

	public String getFull_name() {
		return fullName;
	}

	public Map<String, Boolean> getRoles()
	{
		return this.roles;
	}

}