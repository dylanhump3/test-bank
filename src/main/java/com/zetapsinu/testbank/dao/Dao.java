package com.zetapsinu.testbank.dao;

import com.zetapsinu.testbank.entity.Entity;

import java.util.List;

/**
 * Created by dylanhumphrey on 1/23/17.
*/

/**
 * The highest level for the all Dao's used by this webapp
 * @param <T> The Persistent Entity
 * @param <I> The Object used to represent the Primary key of the Entity
 */
public interface Dao<T extends Entity, I>
{
    List<T> findAll();

    T find(I id);

    T save(T entity);

    void delete(I id);

    void delete(T entity);
}
