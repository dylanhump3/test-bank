package com.zetapsinu.testbank.dao.accesstoken;

import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.dao.Dao;
/**
 * Created by dylanhumphrey on 1/23/17.
 * An interface for the SQL AccessToken table
 * */
public interface AccessTokenDao extends Dao<AccessToken, Long>
{
    AccessToken findByToken(String accessTokenString);
}
