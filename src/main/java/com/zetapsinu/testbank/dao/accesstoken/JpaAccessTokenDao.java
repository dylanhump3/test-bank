package com.zetapsinu.testbank.dao.accesstoken;

import com.zetapsinu.testbank.entity.AccessToken;
import com.zetapsinu.testbank.dao.JpaDao;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Created by dylanhumphrey on 1/23/17.
 * The implementation of the AccessTokenDao for accessing the SQL AccessToken Table
 * */
public class JpaAccessTokenDao extends JpaDao<AccessToken, Long> implements AccessTokenDao
{
    public JpaAccessTokenDao()
    {
        super(AccessToken.class);
    }

    /** Method finds an Accesstoken based on an the token string in the SQL Table
     * @param accessTokenString the token string in SQL
     * @return An AccessToken entity from SQL or Null if not found
     * */
    @Override
    @Transactional(readOnly = true, noRollbackFor = NoResultException.class)
    public AccessToken findByToken(String accessTokenString)
    {
        CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<AccessToken> query = builder.createQuery(this.entityClass);
        Root<AccessToken> root = query.from(this.entityClass);
        query.where(builder.equal(root.get("token"), accessTokenString));

        try {
            return this.getEntityManager().createQuery(query).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
