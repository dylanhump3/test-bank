package com.zetapsinu.testbank.dao;

import com.zetapsinu.testbank.entity.Entity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by dylanhumphrey on 1/23/17.
 * An implementation of the Dao interface which gives access to all necessary JPA components
 * @param <T> The Persistent Entity
 * @param <I> The Object used to define the Primary Key of the persistent entity
 */
public class JpaDao<T extends Entity, I> implements Dao<T, I> {

    /*The JPA bean that manages all entities in the current context*/
    private EntityManager entityManager;

    /*Necessary to know the class of the defined entity in order to run JPA queries*/
    protected Class<T> entityClass;

    /**
     * Main constructor for any JpaDao
     * @param entityClass The Class that this Dao will manage
     */
    public JpaDao(Class<T> entityClass)
    {
        this.entityClass = entityClass;
    }

    /*Simple getter method*/
    public EntityManager getEntityManager()
    {
        return this.entityManager;
    }

    /*Simple getter method*/
    public Class<T> getEntityClass(){
        return this.entityClass;
    }

    /**
     * Will be able to grab the entity manager from teh current application context
     * @param entityManager The JPA entity manager that makes everything possible
     */
    @PersistenceContext
    public void setEntityManager(final EntityManager entityManager)
    {
        this.entityManager = entityManager;
    }

    /**
     * Finds all entities of type T in the database
     * @return A list of all entities found
     */
    @Override
    @Transactional(readOnly = true)
    public List<T> findAll()
    {
        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<T> criteriaQuery = builder.createQuery(this.entityClass);

        criteriaQuery.from(this.entityClass);

        TypedQuery<T> typedQuery = this.getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    /**
     * Finds an entity of type T by its uuid
     * @param id The uuid of the entity
     * @return The entity with the passed in id or null if an entity with that id doesn't exist
     */
    @Override
    @Transactional(readOnly = true)
    public T find(I id)
    {
        return this.getEntityManager().find(this.entityClass, id);
    }

    /**
     * Saves an entity of type T into the database
     * @param entity The entity to save
     * @return The saved entity
     */
    @Override
    @Transactional
    public T save(T entity)
    {
        return this.getEntityManager().merge(entity);
    }

    /**
     * Deletes an entity based on its uuid
     * @param id The uuid of the entity to delete
     */
    @Override
    @Transactional
    public void delete(I id)
    {
        if (id == null) {
            return;
        }

        T entity = this.find(id);
        if (entity == null) {
            return;
        }

        this.getEntityManager().remove(entity);
    }

    /**
     * Deletes an entity of type T
     * @param entity the entity to delete
     */
    @Override
    @Transactional
    public void delete(T entity)
    {
        this.getEntityManager().remove(entity);
    }
}
