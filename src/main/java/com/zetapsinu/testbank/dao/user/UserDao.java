package com.zetapsinu.testbank.dao.user;

import com.zetapsinu.testbank.dao.Dao;
import com.zetapsinu.testbank.entity.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.sql.SQLException;

/**
 * Created by dylanhumphrey on 1/23/17.
 * An interface for the SQL User table
 * */
public interface UserDao extends Dao<User, Long>
{
    User loadUserByUsername(String username) throws UsernameNotFoundException;

    User findByName(String name);

    void updateUser(User user) throws SQLException;

    void deleteUser(User user) throws SQLException;
}
