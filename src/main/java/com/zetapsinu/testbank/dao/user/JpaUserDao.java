package com.zetapsinu.testbank.dao.user;

import com.zetapsinu.testbank.dao.JpaDao;
import com.zetapsinu.testbank.entity.Role;
import com.zetapsinu.testbank.entity.User;
import javafx.util.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;

/**
 * Created by dylanhumphrey on 1/23/17.
 * The implementation of the UserDao for accessing the SQL User Table
 * */
public class JpaUserDao extends JpaDao<User, Long> implements UserDao
{
    private static Logger logger = Logger.getLogger(JpaUserDao.class);

    /* Must autowire the datasource here in order to retrieve the connection to RDS and execute raw SQL commmands*/
    @Autowired
    private DataSource dataSource;

    public JpaUserDao()
    {
        super(User.class);
    }

    /**
     * Grabs a user from the User table based on a username
     * @param username The username to be checking against the database
     * @return A user from SQL with the specified username
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public User loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = this.findByName(username);
        if (null == user) {
            throw new UsernameNotFoundException("The user with name " + username + " was not found");
        }

        return user;
    }

    /**
     * Uses a username to grab a User from SQL. This method is the actual implementation and has the necessary code to do the query
     * @param name The username of the User we want to grab
     * @return the User entity from SQL or Null if a user with the specified username doesnt exits
     */
    @Override
    @Transactional(readOnly = true)
    public User findByName(String name)
    {
        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<User> criteriaQuery = builder.createQuery(this.entityClass);

        Root<User> root = criteriaQuery.from(this.entityClass);
        Path<String> namePath = root.get("name");
        criteriaQuery.where(builder.equal(namePath, name));

        TypedQuery<User> typedQuery = this.getEntityManager().createQuery(criteriaQuery);
        List<User> users = typedQuery.getResultList();

        if (users.isEmpty()) {
            return null;
        }

        return users.iterator().next();
    }

    /**
     * Helper method that will clear all foreign Role keys from the User_role table.
     * @param user The user who's roles you want to delete
     * @throws SQLException
     */
    private void clearRolesForUser(User user) throws SQLException{
        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Role> criteriaQuery = builder.createQuery(Role.class);

        Root<Role> root = criteriaQuery.from(Role.class);
        Path<User> idPath = root.get("User_id");
        criteriaQuery.where(builder.equal(idPath, user.getId()));

        this.getEntityManager().createQuery(criteriaQuery).executeUpdate();

        String query = "DELETE FROM User_roles WHERE User_id = " + user.getId();
        Statement statement = dataSource.getConnection().createStatement();
        statement.executeUpdate(query);
        statement.close();
    }

    /**
     * Helper method that will clear all foreign AccessToken keys from the AccessToken table.
     * @param user The user who's AccessTokens you want to clear
     * @throws SQLException
     */
    private void clearAccessTokensForUser(User user) throws SQLException{
        String query = "DELETE FROM AccessToken WHERE user_id = " + user.getId();
        Statement statement = dataSource.getConnection().createStatement();
        statement.executeUpdate(query);
        statement.close();
    }

    /**
     * Method that deletes the specified user and all foreign keys related to the user
     * @param user The user to be deleted
     * @throws SQLException
     */
    @Override
    @Transactional(readOnly = true)
    public void deleteUser(User user) throws SQLException{
        clearRolesForUser(user);
        clearAccessTokensForUser(user);
        String query = "DELETE FROM User WHERE id = " + user.getId();
        Statement statement = dataSource.getConnection().createStatement();
        statement.executeUpdate(query);
        statement.close();
    }

    /**
     * A method to update specific fields on a SQL User. Currently username and password are not changeable.
     * @param toUser The User object with fields already updated (excluding id, username, and password)
     * @throws SQLException
     */
    @Override
    @Transactional(readOnly = true)
    public void updateUser(User toUser) throws SQLException{
        logger.info("New name: " + toUser.getFull_name());
        logger.info("New user role: " + toUser.getRoles());
        logger.info("Is enabled: " + toUser.isEnabled());
        String fullNameQuery = "UPDATE User SET full_name = '" + toUser.getFull_name() + "' WHERE id = " + toUser.getId();
        String enabledQuery = "UPDATE User SET enabled = " + (toUser.isEnabled() ? 1 : 0) + " WHERE id = " + toUser.getId();
        Statement statement = dataSource.getConnection().createStatement();
        Set<Role> roles = toUser.getRoles();
        clearRolesForUser(toUser);
        for (Role role : roles){
            int i = role.toInt();
            String roleQuery = "INSERT INTO User_roles Values(" + toUser.getId() + "," + i + ")";
            statement.addBatch(roleQuery);
        }
        statement.addBatch(fullNameQuery);
        statement.addBatch(enabledQuery);
        statement.executeBatch();
        statement.close();
    }
}
