import com.amazonaws.services.s3.AmazonS3Client;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.net.URL;
import java.security.ProtectionDomain;

/**
 * Created by dylanhumphrey on 2/13/17.
 * Main class that starts the jetty server programmatically
 */
public class Main {

    public static void main(String[] args) throws Exception{
        Server server = new Server(80);

        WebAppContext context = new WebAppContext();
        context.setContextPath("/");

        ProtectionDomain domain = Main.class.getProtectionDomain();
        URL location = domain.getCodeSource().getLocation();

        context.setWar(location.toExternalForm());

        server.setHandler(context);
        server.start();
        server.join();
    }
}
