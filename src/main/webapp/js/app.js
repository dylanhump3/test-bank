angular.module('gainz', ['ngRoute','ngCookies','ngMaterial','gainz.services'])
.config(['$routeProvider', '$httpProvider', '$locationProvider', function($routeProvider, $httpProvider, $locationProvider) {

	$routeProvider.when('/', {
		templateUrl: 'partials/test-bank.html',
		controller: TestBankController,
	});

    $routeProvider.when('/testbank', {
		templateUrl: 'partials/test-bank.html',
		controller: TestBankController,
	});

	$routeProvider.when('/admin',{
		templateUrl: 'partials/admin.html',
		controller: AdminController,
	});

	$routeProvider.when('/login',{
		templateUrl: 'partials/login.html',
		controller: LoginController,
	});

	$routeProvider.otherwise('/');

	$locationProvider.hashPrefix('!');

    /* Register error provider that shows message on failed requests or redirects to login page on
    * unauthenticated requests */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
    	return {
    		'responseError': function(rejection) {
    			var status = rejection.status;
    			var config = rejection.config;
    			var method = config.method;
    			var url = config.url;

    			if (status == 401) {
    				$location.path( "/login" );
    			} else {
    				$rootScope.error = method + " on " + url + " failed with status " + status;
    			}

    			return $q.reject(rejection);
    		}
    	};
    });

    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
    * as soon as there is an authenticated user */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
    	return {
    		'request': function(config) {
    			var isRestCall = config.url.indexOf('rest') == 0;
    			if (isRestCall && angular.isDefined($rootScope.accessToken)) {
    				var accessToken = $rootScope.accessToken;
    				if (gainzConfig.useAccessTokenHeader) {
    					config.headers['X-Access-Token'] = accessToken;
    				} else {
    					config.url = config.url + "?token=" + accessToken;
    				}
    			}
    			return config || $q.when(config);
    		}
    	};
    });

}]).run(function($rootScope, $location, $cookieStore, UserService) {

	/* Reset error when a new view is loaded */
	$rootScope.$on('$viewContentLoaded', function() {
		delete $rootScope.error;
	});

	$rootScope.hasRole = function(role) {

		if ($rootScope.user === undefined) {
			return false;
		}

		if ($rootScope.user.roles[role] === undefined) {
			return false;
		}

		return $rootScope.user.roles[role];
	};

	$rootScope.logout = function() {
		delete $rootScope.user;
		delete $rootScope.accessToken;
		$cookieStore.remove('accessToken');
		$location.path("/login");
	};

	/* Try getting valid user from cookie or go to login page */
	var originalPath = $location.path();
	$location.path("/login");
	var accessToken = $cookieStore.get('accessToken');
	if (accessToken !== undefined) {
		$rootScope.accessToken = accessToken;
		UserService.get(function(user) {
			$rootScope.user = user;
			$location.path(originalPath);
		});
	}

	$rootScope.initialized = true;
});

function LoginController($scope, $rootScope, $location, $cookieStore, UserService) {

	$scope.rememberMe = false;
	$scope.loginError = false;

	$scope.login = function() {
		UserService.authenticate($.param({username: $scope.username, password: $scope.password}), 
			function(authenticationResult) {
				var accessToken = authenticationResult.token;
				$rootScope.accessToken = accessToken;
				if ($scope.rememberMe) {
					$cookieStore.put('accessToken', accessToken);
				}	
				UserService.get(function(user) {
					$rootScope.user = user;
					$location.path("/test-bank");
				});
		}, 	function(errorResult){
				if (errorResult.status === 401){
					$scope.loginError = true;
					$scope.loginErrorMessage = "Invalid Credentials!"
				}
		});
	};
};

function TestBankController($scope, TestBankService, $mdDialog){

	$scope.toggled = false;
	$scope.courses = TestBankService.query();

	$scope.clicked = function(course, $event){
		$mdDialog.show({
			controller: CourseController,
			templateUrl: 'partials/course-view.html',
			parent: angular.element(document.body),
			locals: {
           		course: course
         	},
         	openFrom: $event.target,
         	closeTo: $event.target,
         	focusOnOpen: false,
			clickOutsideToClose:true
  		});
	}

	$scope.handleFileClick = function(course, folder, file){
		
	}
};

function AdminController($scope, $mdDialog, AdminService){
	$scope.currentRole = "USER";

	$scope.addError = false;
	$scope.addYay = false;

	$scope.clearError = false;
	$scope.clearYay = false;

	$scope.editError = false;
	$scope.editYay = false;

	$scope.roleChange = function(newRole){
		$scope.currentRole = newRole;
		console.log('Changed role to' + $scope.currentRole);
	}

	$scope.clearCache = function(){
		AdminService.clear(function(result){
			if (result.statusCode === "OK"){
				$scope.clearYay = true;
				$scope.clearYayMessage = "Cache successfully cleared!";
				$scope.clearError = false;
			}else{
				$scope.clearError = true;
				$scope.clearErrorMessage = "Something went wrong on the server!"
				$scope.clearYay = false;
			}
		});
	}

	$scope.editUser = function(user){
		$mdDialog.show({
			controller: EditUserController,
			templateUrl: 'partials/editUser.html',
			parent: angular.element(document.body),
			locals: {
           		user: user
         	},
			clickOutsideToClose:true
  		}).then(function(oldUsername){
  			console.log('deleted ' + oldUsername);
  			for (i = 0; i < $scope.allUsers.length; i++){
				var user = $scope.allUsers[i];
				if (user.Username === oldUsername){
					$scope.allUsers.splice(i);
					break;
				}
			}
  		});
	}

	$scope.allUsers = AdminService.query();

	$scope.add = function(u, p, n){
		if (u != null && p != null && n != null && u.length != 0 && p.length != 0 && n.length != 0){
			AdminService.add($.param({username : u,password : p,name : n,role : $scope.currentRole}), function(result){
				if (result.statusCode === "OK"){
					$scope.addYay = true;
					$scope.addYayMessage = "Account successfully added!";
					$scope.addError = false;
					$scope.username = "";
					$scope.password = "";
					$scope.name = "";
					$scope.allUsers.push(result.body);
				}else if (result.statusCode === "INTERNAL_SERVER_ERROR"){
					$scope.addError = true;
					$scope.addErrorMessage = "Something went wrong on the server!"
					$scope.addYay = false;
				}else if (result.statusCode === "BAD_REQUEST"){
					$scope.addError = true;
					$scope.addErrorMessage = "A user with that username already exists!"
					$scope.addYay = false;
				}
			});		
		} else {
			$scope.addError = true;
			$scope.addErrorMessage = "All fields must be filled!"
		}
	}
};

function EditUserController($scope, $mdDialog, user, AdminService){
	$scope.user = user;
	var oldUsername = user.Username;

	$scope.editCurrentRole = user.Roles.includes("ADMIN") ? "ADMIN" : "USER";

	$scope.editRoleChange = function(role){
		$scope.editCurrentRole = role;
	}

	$scope.update = function(){
		$mdDialog.hide();
		AdminService.edit($.param({
			newRole: $scope.editCurrentRole, 
			oldUsername: oldUsername,
			newName: $scope.user.Name,
			enabled: $scope.user.Enabled
		}), function(result){
  				if (result.statusCode === "OK"){
					$scope.editYay = true;
					$scope.editYayMessage = "User successfully updated!";
					$scope.editError = false;
				}else{
					$scope.editError = true;
					$scope.editErrorMessage = "Something went wrong on the server!"
					$scope.editYay = false;
				}
  			});
	}

	$scope.delete = function(){
		$mdDialog.hide(oldUsername);
		AdminService.delete($.param({username: oldUsername}),function(result){
			if (result.statusCode === "OK"){
				$scope.editYay = true;
				$scope.editYayMessage = "User successfully deleted!";
				$scope.editError = false;
			}else{
				$scope.editError = true;
				$scope.editErrorMessage = "Something went wrong on the server!"
				$scope.editYay = false;
			}
		});
	}
}

function CourseController($scope, $mdDialog, course){
	$scope.course = course;

	var selectedChildren = [];

	$scope.close = function(){
		$mdDialog.hide();
	}

	$scope.isSelected = function(name){
		return selectedChildren.indexOf(name) != -1;
	}	

	$scope.handleFileClick = function(path){
		var pathEnd = path.slice(path.lastIndexOf("/"));
		console.log("Got full path: " + path);
		console.log("End path: " + pathEnd);
		if (pathEnd.includes(".")){
			var URL = "https://s3.us-east-2.amazonaws.com/gainz/" + course.name + path;
			var link = URL.replace(/ /g, "+");
			console.log("Opening link: " + link);
			var win = window.open(link, '_blank');
			win.focus;
		}else{
			if ($scope.isSelected(pathEnd.replace("/", ""))){
				selectedChildren.splice(selectedChildren.indexOf(pathEnd.replace("/", "")), 1);
			}else{
				selectedChildren.push(pathEnd.replace("/", ""));
			}
		}
		console.log("selectedChildren: " + selectedChildren);
	}
}

var services = angular.module('gainz.services', ['ngResource']);

services.factory('UserService', function($resource) {

	return $resource('rest/user/:action', {},
	{
		authenticate: {
			method: 'POST',
			params: {'action' : 'authenticate'},
			headers : {'Content-Type': 'application/x-www-form-urlencoded'}
		}
	});
});

services.factory('TestBankService', function ($resource) {

	return $resource('rest/testbank', {});
});

services.factory('AdminService', function($resource){
    return $resource('rest/admin/:action', {},
    {
        add: {
            method: 'POST',
            params: {'action' : 'add'},
            headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        },
        clear: {
        	method: 'POST',
        	params: {'action' : 'clear'},
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        },
        edit: {
        	method: 'POST',
        	params: {'action' : 'edit'},
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        },
        delete: {
        	method: 'POST',
        	params: {'action' : 'delete'},
        	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }
    });
});