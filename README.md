Read Me
===========================

Understanding the remote host setup (Important!)
-----------------------------------
Ok, so the way this works is as follows. There are 3 different Amazon Web Service resources being used on the same VPC to accomplish everything.
Well, what the fuck does that mean. Pretend that there are 3 computers all in the same room hard wired together, so that they can communicate with each
other, but only certain people outside the room have access to them. These 3 servers are RDS, s3, and EC2 and each one serves the following purposes:
- ####RDS
    A MYSQL database which holds all Access tokens, Users, and User_roles. This is the user database which gets checked against on logins into the webapp.
- ###s3 
    This is a very large storage service that stores every single file in the testbank.
- ###EC2
    This holds the executable WAR file (which is this project after being compiled and built) which is a JETTY web server that is constantly running
on a background thread on EC2.

So wait, who has access to what, you ask? I'm confused you say. Well, their are these things called security groups which determines which
ip addresses have access to these services on different protocols. Head on over to the EC2 console in AWS and click on security groups. These
are the current permissions for each service:
- RDS: Allows all connections from within VPC. This means that only the EC2 instance (which houses the web server) has access to the User data. However,
if your looking the security group page, select rds-launch-wizard-2. This is the security group that currently dictates the User database.
You will see ip addresses on there as well as other security groups denoted by sg-4dde5124 (EC2 Security), which gives EC2 access to the RDS database.
The ip addresses correspond my local computer's ip address so I can access them while I'm running the web server locally (which you will be if you want to make code changes).
 You should add your local ip address to this security group if you wish to run the web server locally
- s3: All ip addresses have access to get items from the database. This means that anyone with a link to the file will be able to view it.
Only you have access to add files via the s3 management console
- EC2: All ip addresses have access over the HTTP protocol on the port 80. This means that anyone can access the website, but an account
is needed to log in and actually view the data. However, if you look at the EC2 security group you will see ip addresses are allowed over
the SSH protocol. Add your ip address here so you can deploy to the remote host later

And boom! Hopefully that made some sense, but yeah if you added your ip address to the security groups then your ready to start coding!
Here's what you need to do!

Step 1: git branch creation
---------------------------
This step will explain the preliminary things you need to do in order to get the project on your local IDE on a separate branch. This means
that any code changes won't actually have any effect until I deem they are worthy.

1. Ok, so if you don't know how git works now is the time to learn! Otherwise, skip this step.
2. Using your git knowledge, create a new branch off of the master repository (name it w/e)
3. Clone or pull this branch into your local IDE. I would recommend using IntelliJ as it has built in VCS which makes ur life hella easy
4. Dope, you should now see all the source code in your IDE and in the bottom right (if using IntelliJ) you should see Git: /*Your branch name*/

Step 2: Editing code and testing
--------------------------------
So, with the project open in your IDE, you want to make code changes and fuck around with stuff. But how do I test you ask? How does this work
you ask? Well this step will tell you!

1. Before you make any changes, read relevant comments! They will help explain how things are working so you can actually change stuff without
breaking anything.
2. Cool, make your changes and, assuming you made all the necessary security changes, run the server locally. Simply go into terminal (in your IDE) and type `mvn run:jetty` and that's it! You should see some logging statements and everything
should be working.
4. Navigate to localhost:8080 and you should see the website running locally

Step 3: Merging your branch
----------------------------
This step will tell you how to merge your branch with the master!

1. Using your wonderful git knowledge, submit a merge request to merge your branch with the master repository
2. Let me know you did this and I will take a look at your code and hopefully accept it!
3. Yay!!! Your code is now the stuff that will be used in production

Step 4: Production Deployment
-----------------------------
This step will show you how to build and deploy your code changes to EC2.

1. Go to your local console, either cmd prompt or terminal and prepare to make an ssh connection to EC2. To do this, you will first need
to grab the encryption key from the web chair folder on the google drive called 'testbank.pem'
2. In your local console type `ssh -i "path/to/testbank.pem" ec2-user@ec2-52-14-39-144.us-east-2.compute.amazonaws.com` and now your looking at
the console for the remote linux machine!
3. Get the PID number of the java process by typing `ps -A | grep java`, which will display all the java processes. Find the process 
that the testbank is running on and destroy it by typing `kill -9 <PID>`.
4. Now, delete the previous war file by using the command ```rm testbank-1.0.0-SNAPSHOT.war```
5. Ok, EC2 is ready for us to deploy. Now, go back to your local IDE and go to terminal. type in 'mvn clean install' and then go to the target directory.
You should see a testbank-1.0.0-SNAPSHOT.war file. You will need this.
6. Go back to your terminal window and if you haven't already get out of the SSH connection by typing `exit`. Now put in this command
`scp -i "path/to/testbank.pem" /path/to/testbank-1.0.0-SNAPSHOT.war ec2-user@ec2-52-14-39-144.us-east-2.compute.amazonaws.com:~` . This will
copy over your newly built war file to the EC2 instance!
7. Repeat step 3 to reestablish an SSH connection to the instance. Now type in the following command to start the web sever:
`sudo nohup java -jar testbank-1.0.0-SNAPSHOT.war` which will start the web server on a background thread on the EC2 instance
8. Navigate to http://ec2-52-14-39-144.us-east-2.compute.amazonaws.com/ and you should see the web site up and running!
